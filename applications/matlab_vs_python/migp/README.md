# MIGP Demo

This demo is comprised of three notebooks:
1. `MIGP.ipynb` will introduce MIGP, download an open fMRI dataset for use in the MIGP demo, regress confounds from the data, perform spatial smoothing, and then run group melodic ICA.
2. `matlab_MIGP.ipynb` will perform Matlab MIGP dimension reduction, run group ICA, and then plot the group ICs.
3. `python_MIGP.ipynb` will perform python MIGP dimension reduction, run group ICA, and then plot the group ICs.

The `MIGP.ipynb` notebook will need to be run first to ensure that the necessary data is downloaded and setup.

## Running the notebooks

These notebooks can be run in the `fslpython` environment using:

```
fslpython -m notebook
```

## Dependencies

These notebooks require both `FSL` and `Matlab` to be installed.

### FSL

In addition to FSL you will need to install the python package `nilearn` into the `fslpython` environment with the following command:

> **Note**: `sudo` is required if FSL is installed in an admin/root protected location.


```
sudo $FSLDIR/fslpython/bin/conda install -n fslpython -c conda-forge nilearn

```

### Matlab


In addition to Matlab you will need to know the location of the Matlab executable/binary.  Common matlab paths are below, but they may vary on your system:
```
# MacOS
/Applications/MATLAB_{ver}.app/bin/matlab

# Linux
/usr/local/matlab/{ver}/bin/matlab
```