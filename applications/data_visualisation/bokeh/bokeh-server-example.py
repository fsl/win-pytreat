# ------------------------------
# Run in terminal with command: bokeh serve --show ./bokeh-server-example.py
# ------------------------------

import os
import os.path as op
from subprocess import check_output as run
from tempfile import TemporaryDirectory

import nibabel as nb
from bokeh.layouts import column
from bokeh.models import Button
from bokeh.plotting import figure, curdoc

# load MNI152
FSLDIR = os.environ['FSLDIR']
fname = f'{FSLDIR}/data/standard/MNI152_T1_1mm.nii.gz'

d = nb.load(fname).get_fdata()
d0 = d[:, :, d.shape[2] // 2].squeeze()

# plot center axial slice
p = figure(x_range=(0, 100), y_range=(0, 100), tooltips=[("x", "$x"), ("y", "$y"), ("value", "@image")])
p.image(image=[d0.T], x=0, y=0, dw=100, dh=100, palette="Greys256", level="image")


# create a callback to run BET and show result
def callback():
    with TemporaryDirectory() as tmp:
        bet_name = op.join(tmp, 'betted_img.nii.gz')

        print('running bet')
        out = run(f'bet {fname} {bet_name}'.split())
        print(out)

        dbet = nb.load(bet_name).get_fdata()
        d0bet = dbet[:, :, dbet.shape[2] // 2].squeeze()

        p.image(image=[d0bet.T], x=0, y=0, dw=100, dh=100, palette="Greys256", level="image")


# add a button widget and configure with the call back
button = Button(label="BET Me")
button.on_click(callback)

# put the button and plot in a layout and add to the document
curdoc().add_root(column(button, p))
