#!/usr/bin/env python

# See: https://open.win.ox.ac.uk/pages/fslcourse/lectures/scripting/_0200.fpd/cheat3
PI = 3.1417

def add(a, b):
    return float(a) + float(b)
